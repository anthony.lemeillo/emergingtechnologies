# Self-sovereign identity SSI

By Favre Julien, Le Meillour Anthony, Meichtry Joel, and Pfaffen Kelvin

## Table of Contents

- [Getting started](#getting-started)
    - [What is Self-Sovereign Identity (SSI)?](#what-is-self-sovereign-identity-ssi)
        - [Centralized Identity](#centralized-identity)
        - [Decentralized Identity](#decentralized-identity)
    - [SSI model](#ssi-model)
        - [Issuer](#issuer)
        - [Holder](#holder)
        - [Verifier](#verifier)
- [Proof of Concept](#proof-of-concept)
    - [Background](#background)
    - [Objective](#objective)
    - [Architecture](#architecture)
    - [Implementation](#implementation)
        - [Flask application](#flask-application)
        - [Keycloak](#keycloak)
        - [MattrVII](#mattrvii)
        - [Truvity](#truvity)
    - [Findings and Analysis](#findings-and-analysis)
        - [Decentralized Identity and APIs](#decentralized-identity-and-apis)
        - [HTTPS url with MATTRVII](#https-url-with-mattrvii)
        - [Use mobile wallet](#use-mobile-wallet)
- [Conclusion](#conclusion)

## Getting started

### What is Self-Sovereign Identity (SSI)?

Self-sovereign identity (SSI) is a digital identity framework that enables individuals to own, control, and manage their personal identity information independently. It means that you don't rely on centralized authorities or intermediaries like corporations or government. The main goal is to enhance personal autonomy and privacy.

#### Centralized Identity

Centralized Identity refers to a model of digital identity where an individual's personal information is stored, managed, and controlled by centralized authorities or intermediaries like a third party entity. The users rely on these centralized entities to validate and authenticate their identities. However, this reliance on centralized authorities poses significant risks to personal autonomy and security because the entities have the control over individual's sensitive data and may misuse or mishandle it.

#### Decentralized Identity

Decentralized Identity (DID) represents a more secure and user-centric approach of digital identity management. Each individual has the full ownership, control and management of their personal identity information without the need for intermediaries or centralized authorities. Decentralized Identity enables users to create, manage and authenticate their identities in a secure and private manner. Furthermore, users have the ability to provide selective disclosure of their data, meaning they can choose to share only specific types of information for access, rather than granting access to all their sensitive data at once.

### SSI model

In the SSI model, three key roles are defined: the Holder, the Issuer, and the Verifier:

#### Issuer 
The Issuer is an entity that creates and issues digital credentials to the Holder. This could be a government body issuing a digital driver’s license, an educational institution providing a digital diploma, or a company issuing employment records. Issuers are trusted by the ecosystem participants to provide accurate and reliable assertions about the Holder's attributes or qualifications.

#### Holder
The Holder is the individual or entity that owns and controls these digital credentials. The Holder stores their credentials in a digital wallet, typically a secure application on a mobile device or computer. Holders have the autonomy to decide which pieces of their identity they share and with whom, managing consent and access through cryptographic keys and permissions.

#### Verifier 
The Verifier is an entity that needs to validate the authenticity of the Holder’s credentials. For example, an employer may need to verify a candidate’s educational credentials or a bank may need to confirm a customer's identity before opening a new account. Verifiers use cryptographic techniques to ensure that the credentials presented by the Holder are both genuine and issued by a legitimate Issuer. They also check that the credentials have not been revoked or altered.

![Simple Sequence Diagram](./ressources/Simple-sequence-diagram.png)

In SSI systems, the interactions between these roles are facilitated by technologies such as blockchain and decentralized ledgers, which provide a secure and transparent way to manage and verify credentials without centralized control. The use of verifiable credentials and decentralized identifiers (DIDs) is common, where DIDs allow entities to prove their identity in a privacy-preserving way, and verifiable credentials enable the secure and selective sharing of information.

## Proof of Concept 
Implementing Decentralized Authentication in a Flask Application Using Keycloak, Mattr7, and Truvity

### Background
In 2021, Swiss citizens voted against the e-ID Act, which proposed the establishment of an electronic identity system managed centrally by a private company. The rejection largely stemmed from concerns over privacy, data security, and the centralization of personal information.

### Objective
The primary goal of this proof of concept is to evaluate whether Self-Sovereign Identity (SSI) solutions, specifically through the use of platforms like MattrVII, truly offer decentralized identity management. This investigation seeks to determine if these technologies provide a decentralized framework that sufficiently addresses the security and privacy concerns that led to the rejection of the 2021 Swiss e-ID Act. Furthermore, the project will explore whether such an SSI approach, if found to be centralized, should be considered acceptable to the Swiss public in future legislative proposals.

### Architecture
We use a simple flask application which display personal data. You can connect using keycloak. Then for everything concerning SSI it will be made with the api
### Implementation
#### Flask application
The flask application created for this project will only serve as displaying the desired data provided by the APIs.

First we wanted to use the flask application on localhost with docker, however because the OIDC Client needs a valid https with a valid ssl certificate we needed to deploy the flask app on azure.

For running the flask app you need to create a venv environment in the FlaskApp folder with the command `py -m venv .venv`. Then run the command `.venv\scripts\activate` to activate the venv environment. Then install the needed packages through the command `pip install -r requirements.txt`. 
All keys, credentials etc. can be stored in an .env file and then can be used by the flask app over the command: `os.environ.get`.

For deploying the flask app to azure you need to install [Azure CLI](https://learn.microsoft.com/en-us/cli/azure/install-azure-cli). If necessary, login to Azure using [az login](https://learn.microsoft.com/en-us/cli/azure/authenticate-azure-cli). To deploy the code to Azure you need to run the [az webapp up command](https://learn.microsoft.com/en-us/cli/azure/webapp?view=azure-cli-latest#az-webapp-up): `az webapp up --runtime PYTHON:3.12 --sku B1 --logs`.
You need to set up the environment variables in the settings of the webapp in Azure.

#### Keycloak
KeyCloak is an open-source identity and access management solution developed by Red hat. It provides authentication and authorization services for web applications and mobile applications by acting as centralized authentication server. It supports different authentication protocols such as OAuth 2.0 and OpenID Connect.

First we wanted to use Keycloak on localhost with docker, however there were ssl certificate problems by this approach. Because we needed a valid https with a valid ssl certificate for the OIDC Client, we needed to deploy MattrVII to a server. First we tried to use a Web Application on Azure, however that didn't work. So we asked a school college to host it on his [server](https://webdev-project.ch:8443/). 

For creating a new realm the realm-configuration.json file was used. After that we added some users. Then we changed the fronturl of the new realm to `https://webdev-project.ch:8443/`. Under client flaskapp we needed to add our {{[flask app website](http://nice-forest-8c860925a670438f8c46a289150d1ee8.azurewebsites.net/)}}/callback/* as a valid redirt url and add {{[flask app website](http://nice-forest-8c860925a670438f8c46a289150d1ee8.azurewebsites.net/)}} as web origin.
For creating an OpenID Connect Provider we added the [/.well-known/openid-configuration path of the created OIDC Verifier of MATTR VII tenant](https://joel-meichtry-lvnyqy.vii.au01.mattr.global/ext/oidc/v1/verifiers/10e7e5f2-0957-40e0-9770-4578f66a10b5/.well-known/openid-configuration) to the discovery endpoint. We used the client id and client secret of the MATTR VII tenant as client id and client secret. 

#### MattrVII
MATTRVII is a cloud-native platform specialized in decentralized identify solutions and verifiable data capabilities. The company offers secure, privacy-first solutions that enhance information security and protect data privacy.

1) **Create a trial account**: [MATTR7 trial account](https://mattr.global/get-started)

    - Once your trial account has been created you will receive your Secret from the company.

        ![MATTR7 Secret](./ressources/MATTR7Secret.png)

2) **Install [Postman](https://www.postman.com/downloads/)**

3) **Request to create your bearer token**
    - A bearer token is a type of authentication scheme used to identify the type of token being used for authentication and authorization commonly used with OAuth 2.0 protocol. The token serves as a proof for the authentication to access protected web servers. The bearer keyword helps the server to identify the type of token used, reducing the misinterpretation or miscommunication. This token will be mandatory for the following requests.
    - Create a **POST** request on Postman with the following url: `{{YOUR_AUTH_URL}}/oauth/token`
    - In the body, select **raw** and type this payload:

        ```json
            {
                "client_id": "YOUR_CLIENT_ID",
                "client_secret": "YOUR_CLIENT_SECRET",
                "audience": "YOUR_AUDIENCE",
                "grant_type": "client_credentials"
            }
        ```

    - Click on the "Send" button on the top right. As result, you receive a JSON response with your access token. Your bearer token will expire after 4 hours.

4) **Request to create DIDs**
    - A Decentralized Identity (DID) is a globally unique, available and cryptographically verifiable digital identifier for a specific user. The control of DIDs is given to the users during the use of their digital wallets, which collect verified information about the wallet's owner from certified issuers.
    - Create a **POST** request on Postman with the following url:
    `{{YOUR_AUDIENCE}}/v1/dids`
    - Click on "Authorization", select the type as **Bearer Token** and paste the bearer token obtained during the previous request.
    - In the body, select **raw** and type the following payload:

        ```json
        {
            "method": "key",
            "options": {
                "keyType": "Ed25519"
            }
        }
        ```
    
    - Click on the "Send" button on the top right. As result, you receive a JSON response. The DID is "did": did:key:VALUE
    - This DID key will be useful for us later. You can make a **GET** request with the same URL as the POST request and paste your bearer token in the Authorization section before sending the request.

5) **Request to create a Web sementic**
    - The Web sementic is an extension of the World Wide Web and set by World Wide Web Consortium (W3C) in order to make internet data machine-readable. The data is structured and encoded in standardized format. By this way, software applications understand and analyse the information automatically.
    - Create a **POST** request on Postman with the following url:
    `{{YOUR_AUDIENCE}}/v2/credentials/web-semantic/sign`
    - Click on "Authorization", select the type as **Bearer Token** and paste the bearer token.
    - In the body, select **raw** and type the following payload:

        ```json
        {
            "payload": {
                "name": "Course credential",
                "description": "This credential shows that the person has attended the mention course and attained the relevant awards.",
                "@context": [
                "https://schema.org"
                ],
                "type": [
                "EducationalOccupationalCredential",
                "AlumniCredential"
                ],
                "credentialSubject": {
                "id": "did:example:abcdb1f712ebc6f1c276e12ec21",
                "givenName": "Name",
                "familyName": "family Name",
                "alumniOf": "<span lang=\"en\">Example University</span>"
                },
                "credentialBranding": {
                "backgroundColor": "#B00AA0",
                "watermarkImageUrl": "https://example.edu/img/watermark.png"
                },
                "issuer": {
                "id": "did:key:VALUE",
                "name": "ABC University",
                "logoUrl": "https://example.edu/img/logo.png",
                "iconUrl": "https://example.edu/img/icon.png"
                },
                "expirationDate": "2024-06-01T08:12:38.156Z",
                "issuanceDate": "2024-05-05T08:12:38.156Z"
            },
            "proofType": "Ed25519Signature2018",
            "tag": "identifier123",
            "persist": false,
            "revocable": false,
            "includeId": true
            }
        ```
        - Replace "id": ""did:key:VALUE"," by the value obtained during the previous request.
        - Be sure to replace the issuanceDate with the current date. You can't create a web sementic with a past date.
    - Click on the "Send" button on the top right. As result, you receive a JSON response.

6) **Create an OIDC Verifier**
    - An OpenID Connect (OIDC) is an identity protocol used to verify the identity of a user attempting to gain access to endpoints protected by HTTPS. The OIDC uses the authorization and authentication mechanisms of OAuth 2.0 protocol. One of the benefits of OIDC is its support for single sign-in (SSO) allowing the user to access different endpoints, sites without remembering or re-entering credentials.
    - Create a **POST** request on Postman with the following url:
    `{{YOUR_AUDIENCE}}/ext/oidc/v1/verifiers`
    - Click on "Authorization", select the type as **Bearer Token** and paste the bearer token.
    - In the body, select **raw** and type the following payload:
         ```json
        {
            "verifierDid": "did:key:VALUE",
            "presentationTemplateId": "e22ace5b-7f31-4326-af49-f4fb4a71ea6d",
            "claimMappings": [
                {
                "jsonLdFqn": "http://schema.org/alumniOf",
                "oidcClaim": "alumni_of"
                }
            ],
            "includePresentation": true
        }
        ```
    - Be sure to paste the DID value here : ""did:key:VALUE"," obtained from the step N°4.
    - Click on the "Send" button on the top right. As result, you receive a JSON response.
    - For the following step, you will need the id of the verfier.
    - If you want to get the Verifier's id, simply change the request from POST to **GET** in order to get the id of the verifier.

7) **Create an OIDC Client**
    - Create a **POST** request on Postman with the following url:
    `{{YOUR_AUDIENCE}}/ext/oidc/v1/verifiers/{{VERIFIER_ID}}}}/clients`
    - Click on "Authorization", select the type as **Bearer Token** and paste the bearer token.
    - In the body, select **raw** and type the following payload:
         ```json
        {
            "name": "OIDC Client for the verifier",
            "redirectUris": [
                "https://webdev-project.ch:8443/realms/proofofconcept/broker/oidc/endpoint"
            ],
            "responseTypes": [
                "code"
            ],
            "grantTypes": [
                "authorization_code"
            ],
            "tokenEndpointAuthMethod": "client_secret_post",
            "idTokenSignedResponseAlg": "ES256",
            "applicationType": "web",
            "logoUri": "https://example.com/logo.png"
        }
        ```


#### Truvity
Truvity is a company specialized in the realm of self-sovereign digital identity management. The company provides simple API's that streamline digital wallet complexities, enabling documents exchange and verification.

1) **First create a new API KEY under the [admin portal](https://admin.truvity.com/)** 
    - Don't forget to save the shown value. For every request the api key needed to be added to the header as variable X-API-KEY.

2) **Create an entity**
    - Create a **POST** request with the following url `https://api.truvity.com/api/wallet/v1/entity`

3) **Create a wallet for an entity**
    - Create a **POST** request with the following url `https://api.truvity.com/api/wallet/v1/wallet` and the following params: `'entityDid': {{ENTITYKEY}}`

4) **Create draft credential**
    - Create a **POST** request with the following url `https://api.truvity.com/api/wallet/v1/credential` and the 
    following params: `'walletDid': {{WALLETKEY}},'entityDid': {{ENTITYKEY}}`

5) **Encode JSON data to JSON-LD**
    - With the python command base64.b64encode(JSON_FILE.encode('utf-8')) you can encode JSON to JSON_LD.
    - This is needed to encode this JSON input:
    ```
    {
        "@context": ["https://www.w3.org/2018/credentials/v1", "https://www.w3.org/2018/credentials/examples/v1"],
        "id": {{CREDENTIALKEY}},
        "type": ["VerifiableCredential", "AlumniCredential"],
        "issuer": "https://example.edu/issuers/565049",
        "issuanceDate": "2010-01-01T19:23:24Z",
        "credentialSubject": {
            "alumniOf": {
                "id": "did:example:c276e12ec21ebfeb1f712ebc6f1",
                "name": "Example University"
            }
        }
    }
    ```
    to this JSON_LD output:
    ```
    {
        "JsonLd": "ewogICAgIkBjb250ZXh0IjogWyJodHRwczovL3d3dy53My5vcmcvMjAxOC9jcmVkZW50aWFscy92MSIsICJodHRwczovL3d3dy53My5vcmcvMjAxOC9jcmVkZW50aWFscy9leGFtcGxlcy92MSJdLAogICAgImlkIjogbnVsbCwKICAgICJ0eXBlIjogWyJWZXJpZmlhYmxlQ3JlZGVudGlhbCIsICJBbHVtbmlDcmVkZW50aWFsIl0sCiAgICAiaXNzdWVyIjogImh0dHBzOi8vZXhhbXBsZS5lZHUvaXNzdWVycy81NjUwNDkiLAogICAgImlzc3VhbmNlRGF0ZSI6ICIyMDEwLTAxLTAxVDE5OjIzOjI0WiIsCiAgICAiY3JlZGVudGlhbFN1YmplY3QiOiB7CiAgICAgICAgImFsdW1uaU9mIjogewogICAgICAgICAgICAiaWQiOiAiZGlkOmV4YW1wbGU6YzI3NmUxMmVjMjFlYmZlYjFmNzEyZWJjNmYxIiwKICAgICAgICAgICAgIm5hbWUiOiAiRXhhbXBsZSBVbml2ZXJzaXR5IgogICAgICAgIH0KICAgIH0KfQ=="
    }
    ```

6) **Update a credential**
    - Create a **PATCH** request with the following url `https://api.truvity.com/api/wallet/v1/credential/{{CREDENTIALKEY}}` and the following params: `'walletDid': {{WALLETKEY}},'entityDid': {{ENTITYKEY}}, 'revision': {{CURRENTREVISION}}`
    - Add the JSON_LD output to the body of the request 

7) **Issue a credential**
    - Create a **POST** request with the following url `https://api.truvity.com/api/wallet/v1/credential/{{CREDENTIALKEY}}` and the following params: `'walletDid': {{WALLETKEY}},'entityDid': {{ENTITYKEY}}, 'revision': {{CURRENTREVISION}}`
    - Add the following JSON input to the body of the request
    ```
    {
        "Issuer": {
            "EntityDid": {{ISSUERENTITYKEY}},
            "Revision": 1,
            "WalletDid": {{ISSUERWALLETKEY}}
        }
    }
    ```

8) **Create verifiable presentation draft**
    - Create a **POST** request with the following url `https://api.truvity.com/api/wallet/v1/vp/{{CREDENTIALKEY}}` and the following params: `'walletDid': {{WALLETKEY}},'entityDid': {{ENTITYKEY}}}`

9) **Update verifiable presentation**
     - Create a **PATCH** request with the following url `https://api.truvity.com/api/wallet/v1/vp/{{VPKEY}}` and the following params: `'walletDid': {{WALLETKEY}},'entityDid': {{ENTITYKEY}}, 'revision': {{CURRENTREVISION}}`
    - Add the JSON_LD output to the body of the request 

10) **Add credential to verifiable presentation**
    - Create a **POST** request with the following url `https://api.truvity.com/api/wallet/v1/vp/from-vc/{{VPKEY}}` and the following params: `'walletDid': {{WALLETKEY}},'entityDid': {{ENTITYKEY}}, 'revision': {{CURRENTREVISION}}`
    - Add the following JSON input to the body of the request
    ```
    {
        "CredentialDid": {{CREDENTIALKEY}}
    }
    ```

11) **Issue a verifiable presentation**
    - Create a **POST** request with the following url `https://api.truvity.com/api/wallet/v1/cp/{{VPKEY}}` and the following params: `'walletDid': {{WALLETKEY}},'entityDid': {{ENTITYKEY}}, 'revision': {{CURRENTREVISION}}`

### Findings and Analysis

#### Decentralized Identity and APIs
During our tests, we have discovered that MATTR7 and Truvity aim to offer Decentralized Identifiers (DIDS) for identity management. However, due to our reliance on their APIs for integration purposes, the storage of our public keys occurs within the API's infrastructure. Consequently, while the intention is to use DIDs, the implementation currently aligns more with a Centralized Identity framework, as our public keys are stored and managed by external API services. We have created a sequence diagram that shows all the steps between the Holder, the API, the Issuer and the Verifier:

![Sequence Diagram](./ressources/SequenceDiagramFullFlow.png)

Indeed, the use of API's for requests to the Issuer and the Verifier contradicts the DIDs systems, as it introduces a dependency on centralized external services. In a truly DIDs framework, interactions between the Holder, the Issuer and the Verifier should occur peer-to-peer, without reliance on the intermediaries like the APIs.

#### HTTPS url with MATTRVII
Another issue we had was related to our keycloak and Flask application integration. Both of our applications used an HTTP url to connect to the localhost server. However, to be able to create an OIDC Client we had to redirect an HTTPS url. Even if we had created a non-valid certificate to have an HTTPS url, the request to create the OIDC Client couldn't be achieved because of the non-valid certificate. In the Documentation of [MATTRVII](https://learn.mattr.global/api-reference/latest#tag/OIDC-Verifier-Client), the redirect Uris must have requirements to work:

![RequestUris](./ressources/RequestUris.png)

And even with the url shown for testing, our POST request didn't work with a response of 403 Forbidden error.

We tried multiple solutions to have Keycloak and the Flask application in HTTPS.
- The first solution was to create a Web Application on Azure to have Keycloak in HTTPS. However, each time we tried to browse the application, we encountered the same error:


    ![Application Error](./ressources/AppError.png)

    Everytime we tried to run the web application, instead of having a status of "Running" we had "Succeded".
- Container Instance with docker compose, an official image of keycloak or an image of Keycloak that we build, didn't work because of unknown reasons
- Windows VM, docker didn't run on VM , because of Virtualization problems.
- Linux VM, SSH certificates, that were created by usage of the tool [Certbot](https://certbot.eff.org/), weren't copyable and for that reason not usable.

#### Use mobile wallet
We also tried to send a message to our mobile wallet, which you can download to your smartphone using the [App Store](https://apps.apple.com/us/app/mattr-wallet/id1518660243) or Android using [Google Play](https://play.google.com/store/apps/details?id=global.mattr.wallet).

After obtaining your wallet, you can follow these steps in [MATTRVII](https://learn.mattr.global/api-reference/latest/#tag/Messaging).
We could send a message to our wallet, but if we want to open it, we got the following error:

![Wallet Message Error](./ressources/MobileWallet.jpg)

The same error we encounter by scanning our generated QR-Code.

## Conclusion
During this project, we have discovered the new technology related to Self-Sovereign Identity (SSI) and the theoretical aspects surrounding SSI and Verifiable Credentials. We have also discovered the fundamental concepts of personal autonomy, privacy enhancements and trustless interactions that SSI promises to deliver. Due to its complexity, we needed a lot of research and meetings with Mr. Beuchat to clearly understand how Verifiable Credentials and how the workflow looks like.

However, when transitioning from theory to practical implementation, we encountered significant issues, particularly regarding the necessity of a valid SSL certificate. Without a valid SSL certificate, we cannot redirect Keycloack's authentication to MATTRVII's client OIDC, because the request to create a client OIDC for MATTRVII needs a URL with a valid HTTPS protocol. We also wasted a lot of time trying to make requests to MATTRVII directly on the app.py without any concluant results before using Postman. At the end we couldn't make MATTRVII work with Keycloack, because we didn't get informative errors. So we don't know where the problems are and what needs to be changed that it works. 

Concerning Truvity, the problems was related to the DIDCom Messages and the sending of the certificate that did not work. Furthermore, it was not clear how to combine with Keycloak. This is way we couldn't use Truvity with keycloak.

This practical limitation highlights the complexities involved in implementing SSI solution in real-world scenarios. Despite understanding the theoretical and recognizing the potential benefits, the technical challenges, such as SSL certificate requirements, bring us a lot of obstacles.