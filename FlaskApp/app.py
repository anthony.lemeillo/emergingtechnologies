from flask import Flask, redirect, url_for, session, request
import keycloak
import os
import secrets
import requests
import json
import base64
from dotenv import load_dotenv

load_dotenv()

app = Flask(__name__)
app.secret_key = secrets.token_hex(16)

KEYCLOAK_URL = os.environ.get('KEYCLOAK_URL')
KEYCLOAK_CLIENT_ID = os.environ.get('KEYCLOAK_CLIENT_ID')
KEYCLOAK_CLIENT_PASSWORD = os.environ.get('KEYCLOAK_CLIENT_PASSWORD')
KEYCLOAK_REALM = os.environ.get('KEYCLOAK_REALM')

'''
CREDENTIAL_JSON = os.getenv('CREDENTIAL_JSON')
CREDENTIALDID_JSON = os.getenv('CREDENTIALDID_JSON')
DATA_JSON = os.getenv('DATA_JSON')
DRAFT_CREDENTIAL_KEY = os.getenv('DRAFT_CREDENTIAL_KEY')
IDENTITY_OWNER_KEY = os.getenv('IDENTITY_OWNER_KEY')
ISSUER_KEY = os.getenv('ISSUER_KEY')
ISSUER_REQUEST_BODY = os.getenv('ISSUER_REQUEST_BODY')
SEND_JSON = os.getenv('SEND_JSON')
TRUVITY_API_KEY = os.getenv('TRUVITY_API_KEY')
VERIFIER_KEY = os.getenv('VERIFIER_KEY')
VP_OWNER = os.getenv('VP_OWNER')
WALLET_FOR_IDENTITY_OWNER = os.getenv('WALLET_FOR_IDENTITY_OWNER')
WALLET_FOR_ISSUER = os.getenv('WALLET_FOR_ISSUER')
WALLET_FOR_VERIFIER = os.getenv('WALLET_FOR_VERIFIER')


print(KEYCLOAK_CLIENT_PASSWORD)
print(TRUVITY_API_KEY)
print(CREDENTIAL_JSON)
print(CREDENTIALDID_JSON)
print(DATA_JSON)
print(DRAFT_CREDENTIAL_KEY)
print(IDENTITY_OWNER_KEY)
print(ISSUER_KEY)
print(ISSUER_REQUEST_BODY)
print(KEYCLOAK_CLIENT_PASSWORD)
print(SEND_JSON)
print(TRUVITY_API_KEY)
print(VERIFIER_KEY)
print(VP_OWNER)
print(WALLET_FOR_IDENTITY_OWNER)
print(WALLET_FOR_ISSUER)
print(WALLET_FOR_VERIFIER)
'''


keycloak_openid = keycloak.KeycloakOpenID(server_url=KEYCLOAK_URL,
                                         client_id=KEYCLOAK_CLIENT_ID,
                                         realm_name=KEYCLOAK_REALM,
                                         client_secret_key=KEYCLOAK_CLIENT_PASSWORD,
                                         verify=False)

@app.route('/')
def home():
    return '<p>Welcome to the Emerging technology Flask App</p><a href="/login">Login</a>'


@app.route('/login')
def login():
    auth_url = keycloak_openid.auth_url(redirect_uri=url_for('callback', _external=True), scope='openid')
    return redirect(auth_url)

@app.route('/callback')
def callback():
    code = request.args.get('code')
    if not code:
        return "Authorization code not found in request.", 400

    # Use the backend client for token exchange
    redirect_uri = url_for('callback', _external=True)
    try:
        token_response = keycloak_openid.token(grant_type='authorization_code',
                                               code=code,
                                               redirect_uri=redirect_uri)
    except Exception as e:
        return f"Token exchange failed: {str(e)}", 500

    # Store tokens in session
    session['access_token'] = token_response.get('access_token')
    session['refresh_token'] = token_response.get('refresh_token')
    session['id_token'] = token_response.get('id_token')

    return redirect(url_for('profile'))


@app.route('/profile')
def profile():
    if not session.get('access_token'):
        return "User not logged in.", 401


    try:
        # Retrieve the user's profile
        user_info = keycloak_openid.userinfo(session['access_token'])

        # Display user information
        return (f"<h1>Profile</h1>"
                f"<p>Username: {user_info.get('preferred_username')}</p>"
                f"<p>Email: {user_info.get('email')}</p>"
                f"<p>First Name: {user_info.get('given_name')}</p>"
                f"<p>Last Name: {user_info.get('family_name')}</p>"
                f"<button><a href='/logout'>Logout</a></button>"), 200

    except Exception as e:
        # Handle errors
        return f"Failed to retrieve user profile: {str(e)}", 500


@app.route('/logout')
def logout():
    # Revoke tokens
    keycloak_openid.logout(session['refresh_token'])

    # Clear session
    session.clear()

    return redirect(url_for('home'))

@app.route('/truvity')
def truvity():
    headers = {"Content-Type": "application/json", 'X-API-KEY': TRUVITY_API_KEY}
    '''
    create one entity
    url_entity = 'https://api.truvity.com/api/wallet/v1/entity'
    response = requests.post(url_entity, headers=headers)
    
    get all entities
    url_entity = 'https://api.truvity.com/api/wallet/v1/entities'
    response = requests.get(url_entity, headers=headers)
    
    create wallet for identity owner
    url_wallet = 'https://api.truvity.com/api/wallet/v1/wallet'
    response = requests.post(url_wallet, headers=headers, params={'entityDid': IDENTITY_OWNER_KEY})
    
    create wallet for issuer
    url_wallet = 'https://api.truvity.com/api/wallet/v1/wallet'
    response = requests.post(url_wallet, headers=headers, params={'entityDid': ISSUER_KEY})
    
    get list of wallets of owner
    url_wallet = 'https://api.truvity.com/api/wallet/v1/wallets'
    response = requests.get(url_wallet, headers=headers, params={'entityDid': IDENTITY_OWNER_KEY})
    
    get list of wallets of issuer
    url_wallet = 'https://api.truvity.com/api/wallet/v1/wallets'
    response = requests.get(url_wallet, headers=headers, params={'entityDid': ISSUER_KEY})
    
    create draft credential for owner
    url_credential = 'https://api.truvity.com/api/wallet/v1/credential'
    response = requests.post(url_credential, headers=headers,
                             params={'walletDid': WALLET_FOR_IDENTITY_OWNER, 'entityDid': IDENTITY_OWNER_KEY})
    
    create draft credential for issuer
    url_credential = 'https://api.truvity.com/api/wallet/v1/credential'
    response = requests.post(url_credential, headers=headers,
                             params={'walletDid': WALLET_FOR_ISSUER, 'entityDid': ISSUER_KEY})  
    
    get list of credentials of owner
    url_credential = 'https://api.truvity.com/api/wallet/v1/credentials'
    response = requests.get(url_credential, headers=headers,
                             params={'walletDid': WALLET_FOR_IDENTITY_OWNER, 'entityDid': IDENTITY_OWNER_KEY})             
    
    get list of credentials of issuer
    url_credential = 'https://api.truvity.com/api/wallet/v1/credentials'
    response = requests.get(url_credential, headers=headers,
                             params={'walletDid': WALLET_FOR_ISSUER, 'entityDid': ISSUER_KEY})  
    
    delete credential of owner
    url_credential_to_delete = 'https://api.truvity.com/api/wallet/v1/credential/'+DRAFT_CREDENTIAL_KEY
    response = requests.delete(url_credential_to_delete, headers=headers,
                            params={'walletDid': WALLET_FOR_IDENTITY_OWNER, 'entityDid': IDENTITY_OWNER_KEY})         
    
    update a credential  
    encoded = base64.b64encode(CREDENTIAL_JSON.encode('utf-8'))        
    url_credential_to_update = 'https://api.truvity.com/api/wallet/v1/credential/' + DRAFT_CREDENTIAL_KEY
    response = requests.patch(url_credential_to_update, headers=headers,
                              data=DATA_JSON,
                              params={'walletDid': WALLET_FOR_IDENTITY_OWNER, 'entityDid': IDENTITY_OWNER_KEY,
                                      'revision': 1})                              
    issue a credential
    url_credential_to_update = 'https://api.truvity.com/api/wallet/v1/credential/' + DRAFT_CREDENTIAL_KEY
    response = requests.post(url_credential_to_update, headers=headers,
                              data=ISSUER_REQUEST_BODY,
                              params={'walletDid': WALLET_FOR_IDENTITY_OWNER, 'entityDid': IDENTITY_OWNER_KEY,
                                      'revision': 2})    
                                                     
    list verifiable presentations for owner
    url_verifiable_presentation = 'https://api.truvity.com/api/wallet/v1/vp'
    response = requests.get(url_verifiable_presentation, headers=headers,
                            params={'walletDid': WALLET_FOR_IDENTITY_OWNER, 'entityDid': IDENTITY_OWNER_KEY})
    
    create verifiable presentation for owner
    url_verifiable_presentation = 'https://api.truvity.com/api/wallet/v1/vp'
    response = requests.post(url_verifiable_presentation, headers=headers,
                            params={'walletDid': WALLET_FOR_IDENTITY_OWNER, 'entityDid': IDENTITY_OWNER_KEY})
    
    update verifiable presentation for owner
    url_verifiable_presentation = 'https://api.truvity.com/api/wallet/v1/vp/' + VP_OWNER
    response = requests.patch(url_verifiable_presentation, headers=headers, data=DATA_JSON,
                             params={'walletDid': WALLET_FOR_IDENTITY_OWNER, 'entityDid': IDENTITY_OWNER_KEY,
                                     'revision': 4})                                                                  
    
    add credential to verifiable presentation for owner
    url_verifiable_presentation = 'https://api.truvity.com/api/wallet/v1/vp/from-vc/' + VP_OWNER
    response = requests.post(url_verifiable_presentation, headers=headers, data=CREDENTIALDID_JSON,
                             params={'walletDid': WALLET_FOR_IDENTITY_OWNER, 'entityDid': IDENTITY_OWNER_KEY, 'revision': 3})
                             
    issue verifiable presentation for owner
    url_verifiable_presentation = 'https://api.truvity.com/api/wallet/v1/vp/' + VP_OWNER
    response = requests.post(url_verifiable_presentation, headers=headers,
                             params={'walletDid': WALLET_FOR_IDENTITY_OWNER, 'entityDid': IDENTITY_OWNER_KEY,
                                     'revision': 5})                                        
    
    create wallet for verifier
    url_wallet = 'https://api.truvity.com/api/wallet/v1/wallet'
    response = requests.post(url_wallet, headers=headers, params={'entityDid': VERIFIER_KEY})
    
    send to verifier
    url_send = 'https://api.truvity.com/api/wallet/v1/didcomm/send-plaintext'
    response = requests.post(url_send, headers=headers, data=SEND_JSON,
                             params={'entityDid': IDENTITY_OWNER_KEY, 'walletDid': WALLET_FOR_IDENTITY_OWNER})
                             
    list verifiable presentations of verifier:
    url_verifiable_presentation = 'https://api.truvity.com/api/wallet/v1/vp'
    response = requests.get(url_verifiable_presentation, headers=headers,
                            params={'walletDid': WALLET_FOR_VERIFIER, 'entityDid': VERIFIER_KEY})                      
    
    url_send = 'https://api.truvity.com/api/wallet/v1/didcomm/send-plaintext'
    response = requests.post(url_send, headers=headers, data=SEND_JSON,
                             params={'entityDid': IDENTITY_OWNER_KEY, 'walletDid': WALLET_FOR_IDENTITY_OWNER})
    
     return response.text
    '''

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
